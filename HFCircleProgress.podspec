Pod::Spec.new do |s|

    s.name         = "HFCircleProgress"
    s.version      = "1.0.3"
    s.summary      = "HFCircleProgress is circle progress for tracking percent of consumed nutrients."

    s.description  = "HFCircleProgress is circle progress for tracking percent of consumed nutrients. Can be used in any iOS app. required iOS version 9.0+"

    s.homepage     = "http://healthandfood.azurewebsites.net"
    s.license = { :type => 'MIT', :file => 'MIT-LICENSE.txt' }

    s.author             = "schurik77799@gmail.com"

    s.platform     = :ios, "9.0"
    s.source       = { :git => "https://gitlab.com/dzmitry.antonenka/HFCircleProgress.git", :tag => "1.0.3" }

    s.source_files  = 'HFCircleProgress/**/*.{swift}'
end
