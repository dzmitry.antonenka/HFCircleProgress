//
//  ViewController.swift
//  HFCircleProgress
//
//  Created by Dmitry Antonenka on 2/8/17.
//  Copyright © 2017 BSUIR. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var circleProgress: HFCircleProgress!
    
    @IBOutlet weak var Percent25: UIButton!
    @IBOutlet weak var Percent45: UIButton!
    @IBOutlet weak var Percent75: UIButton!
    @IBOutlet weak var Percent137: UIButton!
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        circleProgress.range = 1.0
        circleProgress.currentValue = 0.5
    }
    
    @IBAction func percentUpdate(_ sender: UIButton) {
        switch sender {
        case Percent25:
            circleProgress.currentValue = 0.25
        case Percent45:
            circleProgress.currentValue = 0.45
        case Percent75:
            circleProgress.currentValue = 0.75
        case Percent137:
            circleProgress.currentValue = 1.37
            
            default:
                return
        }
    }
}

