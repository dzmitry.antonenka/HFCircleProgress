//
//  HFCircleProgress.swift
//  HFCircleProgress
//
//  Created by Dmitry Antonenka on 2/8/17.
//  Copyright © 2017 BSUIR. All rights reserved.
//

import UIKit
import QuartzCore

@IBDesignable
open class HFCircleProgress: UIView {
    let π = CGFloat(M_PI)
    
    fileprivate var _currentValue: CGFloat = 0.0
    fileprivate var _currentRenderingValue: CGFloat = 0.0

    @IBInspectable open var currentValue: CGFloat! {
        willSet(newValue) {
            if newValue > 1.0 { // Outside range. We must notify user anout it
                fgLayerColor = outsideRangleColor
        
                let currentRendVal = newValue - CGFloat(Int(newValue))

                //In case when user entered e.g 2.00(200%)
                //we just convert it to 1.0(100%)
                _currentRenderingValue = currentRendVal != 0 ? currentRendVal : 1.0
                _currentValue = newValue
            } else {
                fgLayerColor = normalFgColor
                _currentValue = newValue
                _currentRenderingValue = newValue
            }
        }
        
        didSet {
            animate()
        }
    }
    
    @IBInspectable open var range: CGFloat = 1.0 {
        willSet {
            assert(range > 0 && range <= 1, "`range` must be in range 0...1")
        }

        didSet {
            configure()
        }
    }
    
    @IBInspectable open var circleRadius: CGFloat = 20.0 {
        didSet {
            configure()
        }
    }
    @IBInspectable open var circleStrokeWidth: CGFloat = 7.0 {
        didSet {
            configure()
        }
    }
    
    @IBInspectable open var fitTextInsideCircle: Bool = false {
        didSet {
            configure()
        }
    }
    @IBInspectable open var textFontSize: CGFloat = 12.0 {
        didSet {
            configure()
        }
    }
    @IBInspectable open var textColor: UIColor = UIColor.white {
        didSet {
            configure()
        }
    }
    
    fileprivate let bgLayer = CAShapeLayer()
    @IBInspectable open var bgLayerColor: UIColor = UIColor.black {
        didSet {
            configure()
        }
    }
    fileprivate let fgLayer = CAShapeLayer()
    @IBInspectable open var outsideRangleColor:UIColor = UIColor.red {
        didSet {
            configure()
        }
    }
    @IBInspectable open var normalFgColor:UIColor = UIColor.red {
        didSet {
            configure()
        }
    }
    

    fileprivate var _fgLayerColor: UIColor = UIColor.white
    @IBInspectable open var fgLayerColor: UIColor = UIColor.white {
        willSet {
            _fgLayerColor = newValue
        }
        
        didSet {
            configure()
        }
    }
    
    fileprivate let percentValueLabel = UILabel()
    
    //MARK: Interface methods
    
    override open func draw(_ rect: CGRect) {
        setup()
    }
    
    open override func awakeFromNib() {
        super.awakeFromNib()
        setup()
        configure()
    }
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        setup(layer: bgLayer)
        setup(layer: fgLayer)
    }
    
    open override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        configure()
    }
    
    func animate() {
        percentValueLabel.text = String.init(format: "%.0f%%", 100.0 * (_currentValue/range))

        let animatableProperty = "strokeEnd"
        let layerAnimatableProperty = "stroke"

        var fromValue = fgLayer.strokeEnd
        let toValue = _currentRenderingValue / range
        if let presentationLayer = fgLayer.presentation() as CAShapeLayer? {
            fromValue = presentationLayer.strokeEnd
        }
        let percentChange = abs(fromValue - toValue)

        let animation = CABasicAnimation(keyPath: animatableProperty)
        animation.fromValue = fromValue
        animation.toValue = toValue
        animation.duration = TimeInterval(percentChange * 2)

        fgLayer.removeAnimation(forKey: layerAnimatableProperty)
        fgLayer.add(animation, forKey: layerAnimatableProperty)
        
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        fgLayer.strokeEnd = toValue
        CATransaction.commit()
    }
    
    //MARK: Private methods
    
    fileprivate func setup() {
        bgLayer.lineWidth = circleStrokeWidth
        bgLayer.strokeColor = bgLayerColor.cgColor
        bgLayer.fillColor = nil
        bgLayer.strokeEnd = 1.0 //set to draw whole path
        self.layer.addSublayer(bgLayer)
        
        fgLayer.lineWidth = circleStrokeWidth
        fgLayer.strokeColor = _fgLayerColor.cgColor
        fgLayer.fillColor = nil
        fgLayer.strokeEnd = 0.0
        self.layer.addSublayer(fgLayer)
    
        //Setup label
        percentValueLabel.translatesAutoresizingMaskIntoConstraints = false
        percentValueLabel.text = "0%"
        percentValueLabel.textColor = textColor
        percentValueLabel.font = UIFont.systemFont(ofSize: textFontSize, weight: UIFontWeightLight)
        
        self.addSubview(percentValueLabel)
        percentValueLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0.0).isActive = true
        percentValueLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0.0).isActive = true
    }
    
    fileprivate func configure() {
        bgLayer.strokeColor = bgLayerColor.cgColor
        fgLayer.strokeColor = fgLayerColor.cgColor
        
        setup(layer: bgLayer)
        setup(layer: fgLayer)
    }
    
    fileprivate func setup(layer shapeLayer: CAShapeLayer) {
        shapeLayer.frame = bounds

        let centerPoint = CGPoint(x: bounds.width/2, y: bounds.height/2)
        let circleOrigin = CGPoint(x: centerPoint.x - circleRadius/2, y: centerPoint.y - circleRadius/2)
        
        let circleRect = CGRect(x: circleOrigin.x, y: circleOrigin.y, width: circleRadius, height: circleRadius)

        let circlePath = UIBezierPath(ovalIn: circleRect)
        shapeLayer.path = circlePath.cgPath
        
        shapeLayer.transform = CATransform3DMakeRotation(-π/2, 0.0, 0.0, 1.0)
    }
}
